<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'role_id' => '1',
            //La password es 'admin' pero tiene que estar encriptada y esto no debería estar aqui 
            'password' => '$2y$10$5m3s2/yASTs6u78YBgKB/eLil8riSLf7h7rV6FIRYGWBtT0647t.K',
            'address' => 'Calle del admin',
            'city' => 'Zaragoza',
            'zip' => '50018',
        ]);
        $user->save();

        $user = new \App\User([
            'name' => 'usuario',
            'email' => 'usuario@usuario.com',
            'role_id' => '2',
            //La password es 'admin' pero tiene que estar encriptada y esto no debería estar aqui 
            'password' => '$2y$10$72e/Bg4musUf7QdEOL7D/u06fHCD.ZadsoYtRHPcpjwkL10uoyN/6',
            'address' => 'Calle del usuario',
            'city' => 'Zaragoza',
            'zip' => '50018',
        ]);
        $user->save();
    }
}
