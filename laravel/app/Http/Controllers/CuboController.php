<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class CuboController extends Controller
{
    public function getIndex(Request $request)
    {
        if($request) {
            $query = $request->get('buscarpor');
            $tipo = $request->get('tipo');
            if($tipo != null) {
                $products = Product::where($tipo, 'LIKE', '%'. $query . '%' )
                ->orderBy('id', 'asc')
                ->get();
            } else {
                $products = Product::all();
                return view('cubos.index', ['products' => $products, 'buscarpor' => $query]);
            }
            return view('cubos.index', ['products' => $products, 'buscarpor' => $query]);
        }
    }

    public function buy(Request $request, $id){
        
        $products = Product::findOrFail($id);
        return view('cubos.buy', compact('products'));
    }

    public function finalbuy(Request $request, $id){
        
        $products = Product::findOrFail($id);
        return view('cubos.finalbuy', compact('products'));
    }
    
    public function finalbuyValidate(Request $request, $id){
        
        // Para el número de las tarjetas de credito
        // Visa (16 dígitos empieza por 4 )
        // Visa (13 dígitos empieza por 4 )
        // MasterCard (16 dígitos empieza por 5 )
           
        $request->validate([
            'username' => 'required|regex:/^[a-zA-Z\s]*$/|min:2',
            'creditNumber' => ['required', 'regex:/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$/'],
            'MM' => ['required', 'regex:/^(1[0-2]|[1-9])$/'],
            'YY' => 'required|regex:/^([2-9][0-9])$/|max:2',
            'CVV' => 'required|regex:/^[0-9]{3}$/', 
        ]);
        $products = Product::findOrFail($id);
        return view('cubos.enhorabuena' , compact('products'));
    }


}
