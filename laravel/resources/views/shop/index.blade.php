@extends('layouts.master')

@section('title')
Witcher's
@endsection

@section('styles')
<!-- <style>
Por si necesito algun style 
</style> -->
@endsection
@section('content')

<!--  BreadCrumps -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb mt-2">
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-home"></i></li>
  </ol>
</nav>
<!-- FIN BreadCrumps -->

<!-- Banner -->
<div class="row my-5">
    <div class="col-lg-12 d-flex justify-content-center">

    <img src="{{ URL::to('img/bannerReyes.jpg') }}"  alt="bannerReyes" title="bannerReyes" width="1172" height="316" class="img-fluid">
    </div>
</div>
<!-- Fin Banner -->
<hr>
<!-- Cartas -->
<div class="row mb-5 mt-5">
    <div class="mb-2 col-lg-2 col-md-6 col-sm-6 offset-lg-3 ">
        <div class="card ">
            <div>
                <h1 class="titulo_card text-center">
                    CUBOS
                </h1>
            </div>
            <a href="/cubo"> 
            <img src="img/cubo3.png" class="card-img-top rounded mx-auto d-block" alt="cuborubik">
            </a>
            <div class="card-body">
                <h5 class="card-title">Cubos de Rubik</h5>
                <p class="card-text ">Una gran variedad de cubos de rubik, ¡con los precios mas bajos del mercado!</p>
                <div>
                <a href="/cubo" role="button" class="boton_vermas float-right btn btn-sm btn-outline-danger">
                        Ver mas -></a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="mb-2 col-lg-2 col-md-6 col-sm-6">
        <div class="card">
            <div>
                <h1 class="titulo_card text-center">
                    PUZZLES
                    
                </h1>
            </div>
            <a href="/cubo"> 
            <img src="img/puzzle.png" class="card-img-top rounded mx-auto d-block" alt="puzzle">
        </a>
            <div class="card-body">
                <h5 class="card-title">Puzzles</h5>
                <p class="card-text ">Puzzles para todas las edades. ¡Desde 10 piezas hasta 9000 piezas!</p>
                    <div>
                <a href="/cubo" role="button" class="boton_vermas float-right btn btn-sm btn-outline-danger">
                        Ver mas -></a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="mb-2 col-lg-2 col-md-6 col-sm-6">
        <div class="card">
            <div>
                <h1 class="titulo_card text-center">
                    Y MAS...
                    
                </h1>
            </div>
            <a href="/cubo"> 
            <img src="img/rompecabezas.png" class="card-img-top rounded mx-auto d-block" alt="rompecabezas">
            </a>
            <div class="card-body">
                <h5 class="card-title">Rompecabezas</h5>
                <p class="card-text ">Rompecabezas de todas las dificultades, desde si eres un iniciado hasta si eres todo un profesional.</p>
                <div>
                <a href="/cubo" role="button" class="boton_vermas float-right btn btn-sm btn-outline-danger">
                        Ver mas -></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fin Cartas -->
<hr>
<!-- ANUNCIOS -->
<div class="row mt-5 mb-5">
    <div class="col-lg-2 col-md-6 col-sm-6 offset-lg-3">
        <div class="text-center">
            <i class="fas fa-dolly fa-3x"></i>
            <h3>Envío Gratis</h3>
            En pedidos superiores a 15€.
        </div>
    </div>
    <div class="col-lg-2 col-md-6 col-sm-6  ">
        <div class="text-center">
            <i class="far fa-handshake fa-3x"></i>
            <h3>Pago con Visa y Mastercard</h3>
            Pago fácil y rápido, sin comisiones.
        </div>
    </div>
    <div class="col-lg-2 col-md-6 col-sm-6 ">
        <div class="text-center">
            <i class="fas fa-shipping-fast fa-3x"></i>
            <h3>Envio express</h3>
            Envio en menos de 48 horas si vives en la Península.
        </div>
    </div>
</div>
<!-- Fin ANUNCIOS -->
<hr>
<!-- TEXTOS -->
<div class="row text-justify mt-5 ">
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 offset-xl-2 offset-lg-2">
    <h2 class="text-center">Tenemos una sección de puzzles</h2>
        <p>Te traemos un gran abanico de puzzles. Encontrarás desde puzzles infantiles hasta los más impresionantes puzzles 3D,
             y por supuesto puzzles de 500, 1000, 1500, 2000 y hasta 9000 piezas,
              con los que podrás decorar tu casa a la vez que te divertirás armando estos bonitos puzzles. </p>
        
    </div>
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
    <h2 class="text-center">Qué cubo de rubik comprar</h2>
        <p>¿Qué estás buscando? ¿Un <strong>cubo de rubik de competición</strong> con el que pulverizar todos los récords? 
        o ¿un <strong>cubo de rubik para principiantes</strong>?. No te preocupes en nuestra web podrás comprar el rubik que 
        mejor se adapte a tus necesidades. Tenemos más de 95 modelos diferentes con precios que van desde el <strong>cubo más barato</strong> 
        por 4,5€ hasta el modelo más avanzado con un novedoso <strong>sistema de posicionamiento magnético</strong> que tiene un precio de 42€.</p>
        
    </div>
</div>
<div class="row text-justify">
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 offset-xl-2 offset-lg-2">
        <h2 class="text-center">Cubos de rubik básicos</h2>
        <p>¿Quieres hacer tu propia colección de cubos y no sabes por dónde empezar? 
            No te preocupes, nosotros te ayudamos. 
            Los modelos que nosotros consideramos básicos son el <strong>cubo mirror</strong>, <strong>pyraminx</strong>,
             <strong>megaminx</strong>, <strong>skewb</strong>, <strong>2x2</strong>,<strong>3x3</strong>,<strong>4x4</strong>,
              <strong>mastermorphix</strong> y <strong>square-1</strong>. Hay muchos más modelos como toda la familia de cuboides,
               pero consideramos que esos son los modelos más esenciales en una colección. </p>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
        <h2 class="text-center">Cómo hacer el cubo de rubik.</h2>
        <p>¿Estás buscando el truco para resolver el cubo de rubik? 
            Armar el cubo es mucho más sencillo de lo que puedes imaginar;
             hay multitud de tutoriales en internet para que aprendas a resolverlo de una forma sencilla con el método para principiantes.
              Aquí tienes el enlace para el <a href="https://www.youtube.com/watch?v=ZB2IPOUGQsg" target="_blank" rel="noreferrer noopener">
              tutorial del cubo de rubik.</a></p>
    </div>
</div>
<div class="row text-justify mb-3">
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 offset-xl-2 offset-lg-2">
        <h2 class="text-center">Récord Cubo de rubik</h2>
        <p>En la actualidad el récord de Single lo tiene Yusheng Du con 3,47 segundos, 
            un tiempo que será muy complicado de mejorar. 
            Y el récord de media lo posee el Australiano <strong>Feliks Zemdegs</strong> con tan solo 5,8 segundos.
            Estos tiempos son impresionantes y sólo están al alcance de los mejores speedcubers del mundo.</p>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 ">
    <h2 class="text-center">Cuál es el mejor cubo de rubik</h2>
        <p>Este es un tema muy delicado; dependiendo del SpeedCuber te dirá que prefiere un modelo u otro.
             Para nosotros, actualmente hay algunos modelos que están por encima del resto en las diferentes categorías de cubos 
             y son <strong>QiYi WuXia 2x2 Magnético</strong>, <strong>Gan 356 Air SM</strong> y <strong>QiYi Wuque 4x4</strong> entre otros.</p>
    </div>
</div> 
<!-- Fin TEXTOS -->
@endsection