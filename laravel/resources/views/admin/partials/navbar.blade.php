<nav class="navbar navbar-expand-lg sticky-top mb-2 navbar-light" style="background-color: white;"> <!-- Esto es para cambiar el color del header backgorund -->
  <a class="navbar-brand" href="/"> <img src="{{ URL::to('img/logo_v1.png') }}" style="max-height: 80px" alt="Logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse navbar-letra" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#"> <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin">Productos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/users">Usuarios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/indexCreate">Crear Producto</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/indexUserCreate">Crear Usuario</a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      @if(Auth::check())
      <li class="nav-item dropdown ml-auto">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user"></i> {{ Auth::user()->name }}
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          
          <a class="dropdown-item" href="{{ route('user.profile') }}"><i class="fas fa-user-circle"></i> Perfil</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('user.logout') }}"><i class="fas fa-sign-out-alt"></i> Cerrar Sesion</a>
        </div>
      </li>
      @else
      <li class="nav-item dropdown ml-auto">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user"></i> Gestion de Usuario
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('user.signin') }}"><i class="fas fa-sign-in-alt"></i> Iniciar Sesion</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('user.signup') }}"><i class="fas fa-user-plus"></i> Crear Usuario</a>
            @endif
        </div>
      </li>
      </ul>
</nav> 