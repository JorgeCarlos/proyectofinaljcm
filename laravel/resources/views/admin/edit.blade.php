@extends('admin.master')

@section('title')
Admin Witcher's
@endsection

@section('styles')
@endsection

@section('content')
<h2>Editar {{ $product->title }}</h2>

<!-- Errores y mensajes -->
@if (session('mensaje'))
<div class="alert alert-success">
    {{ session('mensaje') }}
</div>
@endif

@error('title')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        El titulo es obligatorio
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@enderror
@error('description')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        La descripcion es obligatoria
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@enderror
@error('price')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        El precio es obligatorio o numero no válido
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@enderror
@error('stock')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        El stock es obligatorio o numero no válido
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@enderror
<!-- FIN Errores y mensajes -->

<!-- Formulario editar productos -->
<form action="{{ route('admin.update', $product->id) }}" method="POST">
@method('PUT')
@csrf
  <div class="form-group">
    <label>Titulo</label>
    <input type="text" name="title" class="form-control" value="{{ $product->title }}" >
  </div>
  <div class="form-group">
    <label>Descripción</label>
    <input type="text" name="description" class="form-control" value="{{ $product->description }}">
  </div>
  <div class="form-group">
    <label>Precio</label>
    <input type="number" name="price" class="form-control" value="{{ $product->price }}">
  </div>
  <div class="form-group">
    <label>Stock</label>
    <input type="number" name="stock" class="form-control" value="{{ $product->stock }}">
  </div>
  <button type="submit" class="btn btn-info">Editar</button>
</form>
<!-- FIN Formulario editar productos -->

@endsection