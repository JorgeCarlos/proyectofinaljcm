@extends('admin.master')

@section('title')
Admin Witcher's
@endsection

@section('styles')
@endsection

@section('content')

    <h2>Crear Usuario</h2>

<!-- Errores y mensajes -->

@if (session('mensaje'))
<div class="alert alert-success">
    {{ session('mensaje') }}
</div>
@endif

@error('name')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El nombre no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('email')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
        El email ya está registrado
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('password')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La contraseña no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('address')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La dirección no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('city')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La ciudad no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('zip')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El código postal no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror
<!-- FIN Errores y mensajes -->

<!-- Formulario crear usuario -->
<form action="{{ route('admin.userCreate') }}" method="POST">
    @csrf
    {{ csrf_field() }}
    <div class="form-row">
            <div class="col-lg-6 mb-4">
            <label for="inputname">Nombre </label>
              <input type="name" placeholder="Nombre" id="name"  name="name" class="form-control x p-4 " value="{{ old('name') }}">
            </div>

            <div class="col-lg-6 mb-4">
            <label for="inputEmail">Email</label>
              <input type="email" placeholder="email@email.com" id="email" name="email" class="form-control x p-4" value="{{ old('email') }}">
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-6 mb-4">
            <label for="inputPassword4">Contraseña</label>
              <input type="password" placeholder="Contraseña" id="password" name="password" class="form-control x p-4">
            </div>
            <div class="col-lg-6 mb-4">
            <label for="inputAddress">Dirección</label>
              <input type="text" placeholder="Calle Ejemplo Nº Piso " id="address" name="address" class="form-control x p-4" value="{{ old('address') }}">
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-6 mb-4">
            <label for="inputCity">Ciudad</label>
              <input type="text" placeholder="Cuidad" id="city" name="city" class="form-control x p-4" value="{{ old('city') }}">
            </div>
            <div class="col-lg-6 mb-4">
            <label for="inputZip">Código Postal</label>
              <input type="text" placeholder="Código Postal" id="zip" name="zip" class="form-control x p-4" value="{{ old('zip') }}">
            </div>
          </div>
          <div class="form-row">
          <div class="col-lg-6 mb-4">
            <label for="inputRole_id">Role_id</label>
            <input type="number" placeholder="roleid" name="role_id" class="form-control" value="2" >
            <small>¡Importante!, El rol 1 es solo para administradores, el 2 para usuarios ( viene el 2 por defecto ).</small>
            </div>
      </div>
  <button type="submit" class="btn btn-primary">Crear</button>
</form>
<!-- FIN Formulario crear usuario -->
@endsection