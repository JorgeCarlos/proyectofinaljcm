@extends('admin.master')

@section('title')
Admin Witcher's
@endsection

@section('styles')
@endsection

@section('content')

<!-- Buscador -->
<nav class="navbar navbar-light float-right ">
  <form class="form-inline">
  <select name="tipo" class="form-control mr-lg-2">
    <option value="title">Titulo</option>
    <option value="description">Descripcion</option>
  </select>
    <input name="buscarpor" class="form-control mr-sm-2" type="search" placeholder="Buscar por nombre" aria-label="Search">
    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Buscar</button>
  </form>
  <a href="/admin"><button class="btn btn-outline-primary ml-1 my-2 my-sm-0" type="submit">Resetear Filtros</button></a>
</nav>
<!-- FIN Buscador -->

<h1 class="admin m-3">ESTAS EN LA PAGINA DE ADMINISTRADOR, CUIDADO CON LO QUE TOCAS</h1>

<!-- Mensajes -->
@if (session('mensaje'))
<div class="alert alert-success">
    {{ session('mensaje') }}
</div>
@endif
<!-- FIN Mensajes -->

<!-- Si no hay productos  -->
@if($products->isEmpty())
  <h2>¡No hay productos disponibles!</h2>
@else
<!-- Si hay productos  -->
<h6 class="m-2">
    @if($buscarpor)
    <div class="alert alert-primary" role="alert">
        Los resultados de la busqueda '{{ $buscarpor }}' son:
    </div> 
    @endif
</h6>
@foreach($products->chunk(3) as $productChunk)
<div class="row row-cols-1 row-cols-md-3 row-cols-lg-4 offset-lg-1">
    @foreach($productChunk as $product)
    <div class="col mb-4 lg-3">
        <div class="card">
            <img src="{{ $product->imagePath }}"
                 class="card-img-top rounded my-3 mx-auto d-block"
                 alt="ImagenProducto"
                 onerror="this.src='img/errorImage.png'">

            <div class="card-body">
                <h5 class="card-title">{{ $product->title }}</h5>
                <p class="card-text ">{{ $product->description }}</p>
                <div class="price">{{ $product->price }}€</div>
                <div class="card-text">Stock: {{ $product->stock }}</div>
                <form action="{{ route('admin.delete', $product) }}" method="POST" class="d-inline">
                      @method('DELETE')
                      @csrf
                      <button class="btn btn-danger float-right" type="submit">Eliminar</button>
                </form>
                <a href="{{ route('admin.edit', $product) }}" class="btn btn-info mx-2 float-right">Editar</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endforeach
@endif
<!-- FIN Muestra productos -->
@endsection




