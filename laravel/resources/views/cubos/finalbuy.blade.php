@extends('layouts.master')

@section('title')
Witcher's
@endsection

@section('styles')

<style>
 
.alertaLogin{
  margin: auto;
  width: 50%;
  text-align: center;
}

.cuadradoFondo{
  background-color: white;
  width: 80%;
  height: 7em;
  margin: auto;
  padding-top:7%;
  font-size: 1.2em;
}

</style> 
@endsection

@section('content')

<!-- BreadCrumps -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb mt-2">
    <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="/cubo">Cubos</a></li>
    <li class="breadcrumb-item"><a href="/cubo/buy/{{ $products->id }}">{{ $products->title }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Confirmar Compra {{ $products->title }}</li>
  </ol>
</nav>
<!-- FIN BreadCrumps -->

@if($products->stock <= 0)
<!-- SI NO ESTA EN STOCK  -->
<hr>
  <div class="nostock">Lo sentimos, este artículo se encuentra actualmente fuera de stock</div>
<hr>
@else
<!-- SI ESTA EN STOCK  -->

<!-- Errores y mensajes -->
@if (session('mensaje'))
<div class="mt-2 alertaLogin alert alert-success alert-dismissible fade show" role="alert">
  {{ session('mensaje') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@error('username')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El nombre no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('creditNumber')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
        El número de la tarjeta no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('MM')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El mes de caducidad no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('YY')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El año de caducidad no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('CVV')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El CVV no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror
<!-- FIN Errores y mensajes -->


<div class="row col-lg-11 mb-5">
<!-- Imagen con precio a la izquierda -->
<div class="content-wrapper col-lg-4">	
	<div class="item-container">	
    <img src="{{ URL::to($products->imagePath) }}" 
          id="imagenbuy" 
          class="imagenbuy mx-5 float-left  rounded "
          alt="ImagenProducto">
		<div class="container">	
			<div class="col-md-7 col-lg-7">
				<h2><div>{{ $products->title }}</div></h2>
        <hr>
        <!-- Para el calculo del precio si se necesita cobro del envio -->
        @php 
        if ($products->price >= 15) {
          $total = $products->price;
        } else {
          $total = $products->price + 5;
        }
        @endphp
        @if ($products->price >= 15)
        <div class="cuadradoFondo border border-primary">
        <div class="text-center">{{ $products->price }}€ + 0€ de gastos de envio.</div>
        
        @else
        <div class="cuadradoFondo border border-primary">
        <div class="text-center">{{ $products->price }}€ + 5€ de gastos de envio.</div>
        
        @endif
        <div class="text-center"><strong>Precio total: @php echo $total.'€';  @endphp</strong> </div>
        </div>
			</div>
		</div> 
	</div>
</div>
<!-- FIN Imagen con precio a la izquierda -->


<!-- Formulario de pago -->
<div class="mb-3 col-lg-4">
  <form role="form" action="{{ route('cubo.finalbuyValidate', $products->id  ) }}" method="PUT">
    <div class="form-group">
    <h3 class="text-center ">Pago con tarjeta</h3>
      <label>Nombre del titular</label>
      <input type="text" name="username" placeholder="Nombre Apellidos" class="form-control" value="{{ old('username') }}">
    </div>
    <div class="form-group">
      <label>Numero de la Tarjeta</label>
      <div class="input-group">
      <input type="number"  name="creditNumber" placeholder="Numero de Tarjeta" class="form-control" value="{{ old('creditNumber') }}">
      <div class="input-group-append">
          <span class="input-group-text ">
              <i class="fa fa-cc-visa mx-1"></i>
              <i class="fab fa-cc-mastercard mx-1"></i>
          </span>
        </div>
      </div>
      <small class="form-text text-muted">Solo se aceptan Visa y MasterCard</small>
    </div>
    <div class="row">
      <div class="col-sm-8">
        <div class="form-group">
          <label><span class="hidden-xs">Fecha de Caducidad</span></label>
          <div class="input-group">
            <input type="number" placeholder="MM" name="MM" class="form-control" value="{{ old('MM') }}" >
            <input type="number" placeholder="YY" name="YY" class="form-control" value="{{ old('YY') }}" >
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group mb-4">
          <label data-toggle="tooltip" title="Los tres números detras de la tarjeta">CVV
              <i class="fa fa-question-circle"></i>
          </label>
          <input type="text" name="CVV" class="form-control">
        </div>
      </div>
    </div>
    <button type="submit" type="button" class="subscribe btn btn-primary btn-block rounded-pill shadow-sm">Pagar @php echo $total.'€';  @endphp</button>
    {{ csrf_field() }}
  </form>
</div>
</div>
<!-- FIN Formulario de pago -->
@endif
@endsection