<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Witcher's</title>
    <!-- <link rel="stylesheet" href="src/app.css"> -->
    <link rel="stylesheet" href="{{ URL::to('src/app.css') }}">
    <link rel="shortcut icon" href="{{ URL::to('img/cubo1.png') }}">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/f8fa1c129f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script> 
    <style> 

    .alertaLogin{
      margin: auto;
      width: 50%;
      text-align: center;
    }

    .row_signin{
      background: white;
      border-radius: 5px;
      height: 36rem;
      width: 97%;
      margin:35px;
     
    }
    .otherRow{
      background-color: #f6f7f7;
      border-radius: 5px;
      height: 32rem;
      width: 97%;
      margin-left:1.5%;
      margin-top:1%;
     
    }
    .texto{
    padding: 35px;

    }

    .imagenEnhorabuena{
      max-width: 200px;
      max-height: 200px;
    }

    .subtituloEnhorabuena{
      color:#4040ff;
    }

    </style> 
</head>
<body style="background-color: #F6F7F7;">
<!-- NAVBAR -->
<nav class="navbar navbar-expand-lg sticky-top mb-2 navbar-light" style="background-color: white;"> <!-- Esto es para cambiar el color del header backgorund -->
  <a class="navbar-brand" href="/"> <img src="{{ URL::to('img/logo_v1.png') }}" style="max-height: 80px" alt="Logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse navbar-letra" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href=""> <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/">Inicio</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/cubo">Tienda</a>
      </li>
      @if(Auth::check())
      @if (Auth::user()->role_id == 1)
      <li class="nav-item">
        <a class="nav-link" href="/admin">ZONA SOLO ADMINISTRADORES</a>
      </li>
      @endif
      @endif
      
      </ul>
      <ul class="navbar-nav ml-auto">
      @if(Auth::check())
      <li class="nav-item dropdown ml-auto">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user"></i> {{ Auth::user()->name }}
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          
          <a class="dropdown-item" href="{{ route('user.profile') }}"><i class="fas fa-user-circle"></i> Perfil</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('user.logout') }}"><i class="fas fa-sign-out-alt"></i> Cerrar Sesion</a>
        </div>
      </li>
      @else
      <li class="nav-item dropdown ml-auto">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user"></i> Gestion de Usuario
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('user.signin') }}"><i class="fas fa-sign-in-alt"></i> Iniciar Sesion</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('user.signup') }}"><i class="fas fa-user-plus"></i> Crear Usuario</a>
            @endif
        </div>
      </li>
      </ul>
</nav> 
<!-- FIN NAVBAR -->

<!-- Pantalla de enhorabuena -->
<div class="row_signin border">
  <div class="otherRow border">
    <div class="texto">
    <h3 class="subtituloEnhorabuena">Gracias, has realizado tu pedido con exito.</h3> 
    <p>Compruebe que la información del envio es correcto.
    <p> <strong>Enviar a {{ Auth::user()->name }}, {{ Auth::user()->address }}, {{ Auth::user()->city }}, {{ Auth::user()->zip }}.</strong>  </p> 
    <p>Muchas gracias por comprar en Withercubes. Recibirá su pedido en las proximas 48 horas en su domicilio.</p> 
    <img src="{{ URL::to($products->imagePath) }}" 
			 class="imagenEnhorabuena border offset-lg-3 mx-5 float-left rounded "
       alt="ImagenProducto">
    </div>
    <a href="/cubo"><button class="btn btn-outline-primary " type="submit">Volver a la tienda</button></a>
  </div>
</div>
<!-- FIN Pantalla de enhorabuena -->

<!-- FOOTER -->
<footer class="footer mt-auto py-3">
  <div class="container">
  <div class="row">
          <div class="col-12 col-md">
          <img src="{{ URL::to('img/logo_v1.png') }}" style="max-height: 50px" alt="Logo">
            <div>Tu tienda de Cubos de Rubik, Puzzles y todo lo que puedas imaginar.</div>
          </div>
          <div class="col-6 col-md">
            <h5><i class="fas fa-info"></i> Informacion sobre la tienda </h5>
            <ul class="list-unstyled text-small">
              <li>Witchercubes.com, Calle falsa N123, 42040, (Zaragoza), España</li>
              <li>Llamanos: <a class="footerNumEmail" href="{{ route('loading.index') }}">694 584 123</a></li>
              <li>Email: <a class="footerNumEmail" href="{{ route('loading.index') }}">info@witchercubes.com</a> </li>
              <li></li>
              <li></li>
              <li></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Quienes Somos <i class="fas fa-users"></i></h5>
            <ul class="list-unstyled text-small">
              <li><a class="" href="{{ route('loading.index') }}">Aviso Legal</a></li>
              <li><a class="" href="{{ route('loading.index') }}">Condiciones legales</a></li>
              <li><a class="" href="{{ route('loading.index') }}">Política de privacidad</a></li>
              <li><a class="" href="{{ route('loading.index') }}">Política de devoluciones</a></li>
              <li><a class="" href="{{ route('loading.index') }}">Blog</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Redes Sociales <i class="fas fa-hashtag"></i></h5>
            <ul class="list-unstyled text-small">
              <li><a class="" href="https://www.instagram.com">Instagram <i class="fab fa-instagram"></i></a></li>
              <li><a class="" href="https://www.facebook.com">Facebook <i class="fab fa-facebook-f"></i></a></li>
              <li><a class="" href="https://www.twitter.com">Twitter <i class="fab fa-twitter"></i></a></li>
              <li><a class="" href="https://www.youtube.com">Youtube <i class="fab fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
  </div>
</footer>
<!-- FIN FOOTER -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>  -->
 <!-- <script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min.js"></script>  -->
</body>
</html>