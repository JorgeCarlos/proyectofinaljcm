@extends('layouts.master')

@section('title')
Editar Perfil
@endsection

@section('styles')

<style>
.row_signin{
  width: 100%;
  background: white;
  box-shadow: 12px 12px 22px grey;
}
.boton_sign{
  border:none;
  outline: none;
  height: 50px;
  width: 100%;
  background-color: black;
  color:white;
  border-radius: 4px;
  font-weight: bold;
}

.boton_sign:hover{
  background: white;
  border: 1px solid;
  color:black;
}

.nombreinput{
  margin-top: 15%;
}

.alertaLogin{
  margin: auto;
  width: 50%;
  text-align: center;
}

</style>
@endsection

@section('content')
<!-- BreadCrumps -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb mt-2">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="/user/profile">Perfil de {{ Auth::user()->name }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Editar Perfil de {{ Auth::user()->name }}</li>
  </ol>
</nav>
<!-- FIN BreadCrumps -->

<!-- Errores y mensajes -->
@if (session('mensaje'))
<div class="mt-2 alertaLogin alert alert-success alert-dismissible fade show" role="alert">
  {{ session('mensaje') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@error('name')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El nombre no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('email')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
        El email ya esta registrado
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('password')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La contraseña no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('address')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La dirección no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('city')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La ciudad no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('zip')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El codigo postal no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

<!-- FIN Errores y mensajes -->

<!-- SECTION DE EDITAR -->
<section class="Form mb-5 mt-5">
  <div class="container">
    <div class="row row_signin no-gutter">
      <div class="col-lg-12 px-5 pt-5">
        <h1 class="font-weight-bold py-3"><img src="{{ URL::to('img/logo_v1.png') }}" class="logoimagen" style="max-height: 53px" alt="Logo"></h1>
        <h3>Editar Perfil</h3>
        <form action="{{ route('user.update', Auth::user()->id ) }}" method="POST">
           @method('PUT')
          @csrf
          <div class="form-row">
            <div class="col-lg-6 mb-4">
            <label for="inputname">Nombre </label>
              <input type="name" id="name"  name="name" class="form-control x p-4 " value="{{ Auth::user()->name }}">
            </div>
            <div class="col-lg-6 mb-4">
            <label for="inputEmail">Email</label>
              <input type="email" id="email" name="email" class="form-control x p-4" value="{{ Auth::user()->email }}">
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-12 mb-4">
            <label for="inputAddress">Dirección</label>
              <input type="text" id="address" name="address" class="form-control x p-4" value="{{ Auth::user()->address }}">
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-6 mb-4">
            <label for="inputCity">Ciudad</label>
              <input type="text" id="city" name="city" class="form-control x p-4" value="{{ Auth::user()->city }}">
            </div>
            <div class="col-lg-6 mb-4">
            <label for="inputCity">Codigo Postal</label>
              <input type="text" id="zip" name="zip" class="form-control x p-4" value="{{ Auth::user()->zip }}">
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-12">
              <button type="submit" class="btn1 boton_sign mt-3 mb-4">Editar</button>
            </div>
          </div>
          {{ csrf_field() }}
        </form>
        <form action="{{ route('user.delete', $user) }}" method="POST" class="d-inline">
          @method('DELETE')
          @csrf
          <button class="btn mb-4 btn-danger float-right" type="submit">Eliminar Usuario</button>
        </form>
        <h5 class="form-text text-muted float-right mr-2"> ¡Cuidado! Si borra el usuario será irreversible  </h5>
      </div>
    </div>
  </div>
</section>
<!-- FIN SECTION DE EDITAR -->
@endsection