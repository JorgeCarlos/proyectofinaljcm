@extends('layouts.master')

@section('title')
Inicio Sesion
@endsection

@section('styles')

<style>
.row_signin{
  background: white;
  border-radius: 30px;
  box-shadow: 12px 12px 22px grey;
}

.img_sign{
  margin-left: -4%;
  border-top-left-radius: 30px;
  border-bottom-left-radius: 30px;
}

.boton_sign{
  border:none;
  outline: none;
  height: 50px;
  width: 100%;
  background-color: black;
  color:white;
  border-radius: 4px;
  font-weight: bold;
}

.boton_sign:hover{
  background: white;
  border: 1px solid;
  color:black;
}

.logoimagen{
  margin-left: -3%;
}

.alertaLogin{
  margin: auto;
  width: 45%;
  text-align: center;
}

/* Para cambiar el color de los placeholders del formulario */
#email::placeholder,
#password::placeholder{
  color:#989898;
}

#email,
#password{
  color:#101010;
}
/* FIN placeholders */

</style> 
@endsection

@section('content')

<!-- Errores y mensajes -->
@if ($message = Session::get('error'))
<div class="alertaLogin alert alert-danger alert-dismissible fade show mt-4" role="alert">
    El correo electrónico o la contraseña no coinciden con nuestros registros. Por favor, revise e inténtalo de nuevo.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@error('email')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El email es incorrecto
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('password')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La contraseña es incorrecta
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror
<!-- FIN Errores y mensajes -->

<!-- Section del formulario inicio sesion -->
<section class="Form my-5">
  <div class="container">
    <div class="row row_signin no-gutter">
      <div class="col-lg-5">
          <img src="{{ URL::to('img/erno.jpg') }}" alt="imagen" class=" img_sign img-fluid">
      </div>
      <div class="col-lg-7 px-5 pt-5">
        <h1 class="font-weight-bold py-3"><img src="{{ URL::to('img/logo_v1.png') }}" class="logoimagen" style="max-height: 70px" alt="Logo"></h1>
        <h4>Inicia sesión </h4>
        <form action="{{ route('user.signin') }}" method="post">
          <div class="form-row">
            <div class="col-lg-7">
              <input type="email" placeholder="email@email.com" id="email" value="{{ old('email') }}" name="email" class="form-control my-3 p-4 ">
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-7">
              <input type="password" placeholder="***********" id="password" name="password" class="form-control my-3 p-4">
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-7">
              <button type="submit" class="btn1 boton_sign mt-3 mb-4">Iniciar sesión</button>
            </div>
          </div>
          <p>¿No tienes cuenta? <a href="{{ route('user.signup') }}">¡Regístrate!</a></p>
          {{ csrf_field() }}
        </form>
      </div>
    </div>
  </div>
</section>
<!-- FIN Section del formulario inicio sesion -->

@endsection